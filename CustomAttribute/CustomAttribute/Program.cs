﻿using System;
using System.Reflection;
using System.Linq;
namespace CustomAttribute {

    internal class TestAttribute : Attribute { }
    internal class TestMethodAttribute : Attribute { }

    [Test]
    internal class MyTestSuite {
        public void HelperMethod() {
            //Console.WriteLine("This method is never invoked...");
        }
        [TestMethod]
        public void MyTestMethod1() {
            HelperMethod();
            Console.WriteLine("Doing some testing...");
        }
        [TestMethod]
        public void MytestMethod1() {
            HelperMethod();
            Console.WriteLine("Doing some other testing...");
        }
    }

    internal class MainClass {

        private static void Main(string[] args) { 

            var testsuite = from t in Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetCustomAttributes().Any(a => a is TestAttribute)
                            select t;

            foreach(Type t in testsuite){

                Console.WriteLine("Running tests in suiet: " + t.Name);
                var testMethods = from m in t.GetMethods()
                                  where m.GetCustomAttributes(false).Any(a => a is TestMethodAttribute)
                                  select m;
                object testSuiteInstance = Activator.CreateInstance(t);
                foreach (MethodInfo mInfo in testMethods) {
                    mInfo.Invoke(testSuiteInstance, new object[0]);
                }
            }
            Console.ReadKey();
        }
    }
}