﻿using System;

namespace DelegatesAndEvents
{
    public class WorkPerformedEventArgs : EventArgs
    {
        public WorkPerformedEventArgs(int hours, Worktype worktype)
        {
            Hours = hours;
            WorkType = worktype;
        }

        public int Hours { get; set; }

        public Worktype WorkType { get; set; }
    }
}