﻿using System;
using System.Threading;

namespace DelegatesAndEvents
{
    //public delegate int WorkPerformedHandler(object sender, WorkPerformedEventArgs e );

    public class Worker
    {
        public event EventHandler<WorkPerformedEventArgs> WorkPerformed;

        public event EventHandler WorkCompleted;

        public void Dowork(int hours, Worktype worktype)
        {
            for (int i = 0; i < hours  ; i++)
            {
                // Raise Event here when work is performed.
                Thread.Sleep(1000);
                OnWorkPerformed(i + 1, worktype);
            }

            // Raise event when work is completed.
            OnWorkCompleted();
        }

        protected virtual void OnWorkPerformed(int hours, Worktype worktype)
        {
            var del = WorkPerformed;
            if (del != null)
            {
                del(this, new WorkPerformedEventArgs(hours, worktype));
            }
        }

        protected virtual void OnWorkCompleted()
        {
            var del = WorkCompleted;
            if (del != null)
            {
                del(this, EventArgs.Empty);
            }
        }
    }
}