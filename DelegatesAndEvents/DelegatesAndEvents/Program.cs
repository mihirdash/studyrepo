﻿using System;

namespace DelegatesAndEvents
{
    //public delegate int WorkPerformedHandler(int hrs, Worktype worktype);

    public delegate int CalculationRuleDelegate(int x, int y);
    internal class Program
    {
        private static void Main(string[] args)
        {
            //WorkPerformedHandler del1 = new WorkPerformedHandler(workperformed1);
            //WorkPerformedHandler del2 = new WorkPerformedHandler(workperformed2);
            //WorkPerformedHandler del3 = new WorkPerformedHandler(workperformed3);

            //del1(2, Worktype.GoToMeetings);

            //del1 += del2 + del3;

            //int finalhrs = del1(1, Worktype.GiveADemo);
            //Console.WriteLine(finalhrs);
            var data = new ProcessData();

            //CalculationRuleDelegate addvalue = (x, y) => x + y;
            //CalculationRuleDelegate multiplyvalue = (x, y) => x * y;
            //data.process(2, 3, addvalue);

            //Action<int, int> addaction = (x, y) => Console.WriteLine(x + y);
            //Action<int, int> multiplyaction = (x, y) => Console.WriteLine(x * y);
            //data.processAction(2, 3, addaction);

            Func<int, int, int> addfunc = (x, y) => x + y;
            Func<int, int, int> multiplyfunc = (x, y) => x * y;

            Console.WriteLine(data.processFunc(5, 6, multiplyfunc));

            //var worker = new Worker();
            //worker.WorkPerformed += (s, e) => Console.WriteLine("Hours Worked " + e.Hours + " " + e.WorkType);

            //worker.Dowork(8, Worktype.CreateAnApplication);
            Console.ReadKey();
        }


        //static void Worker_WorkPerformed(object sender, WorkPerformedEventArgs e)
        //{
        //    Console.WriteLine("Hours Worked " + e.Hours + " " + e.WorkType);
        //}

        //static void Worker_WorkCompleted(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Worker is done");
        //}

        //private static int workperformed1(int workhours, Worktype wtype)
        //{
        //    Console.WriteLine("WorkPerformed1 Called " + wtype.ToString() + " for " + workhours.ToString() + " hrs");
        //    return workhours + 1;
        //}

        //private static int workperformed2(int whrs, Worktype typeofwork)
        //{
        //    Console.WriteLine("Workperformed2 Called " + typeofwork.ToString() + " for " + whrs.ToString() + " hrs");
        //    return whrs + 2;
        //}

        //private static int workperformed3(int whrs, Worktype typeofwork)
        //{
        //    Console.WriteLine("Workperformed3 Called " + typeofwork.ToString() + " for " + whrs.ToString() + " hrs");
        //    return whrs + 3;
        //}

        
    }

    public enum Worktype
    {
        GoToMeetings,
        GiveADemo,
        CreateAnApplication
    }
}