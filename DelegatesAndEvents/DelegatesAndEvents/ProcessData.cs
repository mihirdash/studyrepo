﻿using System;

namespace DelegatesAndEvents
{
    public class ProcessData
    {
        public void process(int x, int y, CalculationRuleDelegate del)
        {
           var result = del(x, y);
           Console.WriteLine(result);
        }

        public void processAction(int x, int y, Action<int, int> action)
        {
            action(x, y);
            Console.WriteLine("Action is processed");
        }

        public int processFunc(int x, int y, Func<int, int, int> processFunc)
        {
            return processFunc(x, y);
        }
    }
}